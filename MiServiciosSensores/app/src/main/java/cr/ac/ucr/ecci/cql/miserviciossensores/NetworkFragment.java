package cr.ac.ucr.ecci.cql.miserviciossensores;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

// Implementacion del fragmento que corre la AsyncTask para extraer los datos desde le red
public class NetworkFragment extends Fragment {

    public static final int CANTIDAD_CARACTERES_LEER = 50000;
    public static final String TAG_FRAGMENT = "NetworkFragment";
    private static final String URL_KEY = "UrlKey";

    // Clase que implementa AsyncTask
    private DownloadTask mDownloadTask;

    // Clase para la administracion de las respuestas de la AsyncTask
    private DownloadCallback mCallback;
    private String mUrlString;

    // Iniciamos el fragmento indicando el url desde donde debe bajar el contenido
    public static NetworkFragment getInstance(FragmentManager fragmentManager, String url) {
        // Recuperamos el NetworkFragment en el caso que se este recreando la actividad y no haya finalizado. De esta manera puede continuar con el proceso
        NetworkFragment networkFragment = (NetworkFragment) fragmentManager.findFragmentByTag(NetworkFragment.TAG_FRAGMENT);
        // si el fragmento es null entonces creamos la solicitud con el url especifico
        if (networkFragment == null) {
            networkFragment = new NetworkFragment();
            Bundle args = new Bundle();
            args.putString(URL_KEY, url);
            networkFragment.setArguments(args);
            fragmentManager.beginTransaction().add(networkFragment, TAG_FRAGMENT).commit();
        }
        // retornamos el fragmento
        return networkFragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // El fragmento es retenido aunque existan cambios en configiracion en la Activity  padre
        setRetainInstance(true);
        // obtenemos el parametro para conocer el url al que se le realiza la peticion
        mUrlString = getArguments().getString(URL_KEY);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // La Activity padre puede manejar llamadas callbacks de la tarea
        //Lo hacemos mediante el contexto que tenemos de la interface DownloadCallback
        mCallback = (DownloadCallback)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Limpia las referencias a la Activity padre
        mCallback = null;
    }
    @Override
    public void onDestroy() {
        // Cancela la tarea cuando el Fragment se destruye
        cancelDownload();
        super.onDestroy();
    }

    // Inicia la ejecucion de la tarea asincronica DownloadTask sin bloquear la app
    public void startDownload() {
        cancelDownload();
        mDownloadTask = new DownloadTask();
        mDownloadTask.execute(mUrlString);
    }

    // Cancela y/o interrupte el download en la tarea async
    public void cancelDownload() {
        if (mDownloadTask != null) {
            mDownloadTask.cancel(true);
            mDownloadTask = null;
        }
    }

    // Clase que implementa AsyncTask
    // Implementa la tarea DownloadTask que corre las operaciones de la red en un thread
    private class DownloadTask extends AsyncTask<String, Integer, DownloadTask.Result> {
        // La clase Result obtiene el resultado de la tarea asincronica que sirve para unir los  resultados y excepciones (si las hay).
        // Cuando la tarea download se completa retorna los resultados al thread de UI, el thread de UI fue el que llama a la tarea en el startDownload()
        // que crea la DownloadTask() y la ejecuta mDownloadTask.execute(mUrlString) durante el doInBackground()
        class Result {
            public String mResultValue;
            public Exception mException;
            public Result(String resultValue) {
                mResultValue = resultValue;
            }
            public Result(Exception exception) {
                mException = exception;
            }
        }

        // Antes de ejecutar se verifica que se tenga acceso a la red, si no existe no se ejecuta
        @Override
        protected void onPreExecute() {
            if (mCallback != null) {
                NetworkInfo networkInfo = mCallback.getActiveNetworkInfo();
                if (networkInfo == null || !networkInfo.isConnected() ||  (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                        && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                    // Si no hay conectividad, cancelamos la tarea y actualizamos el Callback a null.
                    mCallback.updateFromDownload(null);
                    cancel(true);
                }
            }
        }

        // Define el trabajo que realizamos durante la ejecucion del background thread
        @Override
        protected Result doInBackground(String... urls) {
            Result result = null;
            if (!isCancelled() && urls != null && urls.length > 0) {
                // obtenemos el url
                String urlString = urls[0];
                try {
                    URL url = new URL(urlString);
                    // llamamos a la operacion de bajar el url
                    String resultString = downloadUrl(url);
                    // Si obtenemos datos los enviamos al resultado
                    if (resultString != null) {
                        result = new Result(resultString);
                    } else {
                        throw new IOException("No hay respuesta recibida.");
                    }
                } catch(Exception e) {
                    result = new Result(e);
                }
            }
            return result;
        }

        // Actulizamos en estado de el progreso de download del publishProgress
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (values.length >= 2) {
                mCallback.onProgressUpdate(values[0], values[1]);
            }
        }

        // Obtenemos el resultado del download y actualizamos el resultado
        @Override
        protected void onPostExecute(Result result) {
            if (result != null && mCallback != null) {
                if (result.mException != null) {
                    mCallback.updateFromDownload(result.mException.getMessage());
                } else if (result.mResultValue != null) {
                    mCallback.updateFromDownload(result.mResultValue);
                }
                // terminamos el download
                mCallback.finishDownloading();
            }
        }

        // Si requerimos realizar algo particular en el cancel lo hacemos aca
        @Override
        protected void onCancelled(Result result) { }
        // Implementamos el metodo para bajar el contenido del url, este establece la conexion http
        // y obtiene la respuesta desde el servidor. Si la solicitud a la red es exitosa retorna el contenido del body en un String
        private String downloadUrl(URL url) throws IOException {
            // declaramos las variables para ontener el resultado y establecer la conexion
            InputStream stream = null;
            HttpsURLConnection connection = null;
            String result = null;
            // establecemos la conexion
            try {
                // abrimos la conexion
                connection = (HttpsURLConnection) url.openConnection();
                // Timeout para la lectura delInputStream // cambiar de acuerdo al servicio y el contenido
                connection.setReadTimeout(3000);
                // Timeout para la conexion
                connection.setConnectTimeout(3000);
                // Establecemos el metodo HTTP como GET
                connection.setRequestMethod("GET");
                // La solicitud conlleva un input (response) body
                connection.setDoInput(true);
                // Abrimos la conexion con la red
                connection.connect();
                // Actualizamos el estado de la tarea
                publishProgress(DownloadCallback.Progress.CONNECT_SUCCESS);
                // si la conexion esta ok
                int responseCode = connection.getResponseCode();
                if (responseCode != HttpsURLConnection.HTTP_OK) {
                    // error en conexion
                    throw new IOException("HTTP error code: " + responseCode);
                }
                // Obtenemos el body de respuesta como un InputStream
                stream = connection.getInputStream();
                // Actualizamos el estado de la tarea
                publishProgress(DownloadCallback.Progress.GET_INPUT_STREAM_SUCCESS, 0);
                // Si el resultado es distinto a null
                if (stream != null) {
                    // Convertimos el Stream a String, extrae solo el valor de  CANTIDAD_CARACTERES_LEER
                    result = readStream(stream, NetworkFragment.CANTIDAD_CARACTERES_LEER);
                    // Actualizamos el estado de la tarea
                    publishProgress(DownloadCallback.Progress.PROCESS_INPUT_STREAM_SUCCESS, 0);
                }
            } finally {
                // Cerramos el Stream y desconectamos la conexion HTTP
                if (stream != null) {
                    stream.close();
                }
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return result;
        }

        // Metodo para convertir un InputStream a String
        private String readStream(InputStream stream, int maxLength) throws IOException {
            String result = null;
            // Leer el InputStream usando UTF-8
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            // Creamos el buffer con el CANTIDAD_CARACTERES_LEER
            char[] buffer = new char[maxLength];
            // Llenamos el buffer con los datos del Stream
            int numChars = 0;
            int readSize = 0;
            while (numChars < maxLength && readSize != -1) {
                numChars += readSize;
                int pct = (100 * numChars) / maxLength;
                // Actualizamos el estado de la tarea
                publishProgress(DownloadCallback.Progress.PROCESS_INPUT_STREAM_IN_PROGRESS, pct);
                readSize = reader.read(buffer, numChars, buffer.length - numChars);
            }
            if (numChars != -1) {
                // Creamos el string
                numChars = Math.min(numChars, maxLength);
                result = new String(buffer, 0, numChars);
            }
            return result;
        }
    }
}
