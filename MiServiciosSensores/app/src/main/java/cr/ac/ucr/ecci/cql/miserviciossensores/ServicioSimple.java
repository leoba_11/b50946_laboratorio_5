package cr.ac.ucr.ecci.cql.miserviciossensores;

import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class ServicioSimple extends IntentService {

    public static final String SERVICE_TAG = "cr.ac.ucr.ecci.cql.mirestsoap.ServicioSimple.ServiceTag";
    public static final String SERVICE_RESULT = "cr.ac.ucr.ecci.cql.mirestsoap.ServicioSimple.REQUEST_PROCESSED";
    public static final String SERVICE_MESSAGE = "cr.ac.ucr.ecci.cql.mirestsoap.ServicioSimple.COPA_MSG";

    // Constructor
    public ServicioSimple() {
        super(ServicioSimple.SERVICE_TAG);
    }

    // Este metodo implementa el trabajo que debe realizar el servicio
    @Override
    protected void onHandleIntent(Intent intent) { // Obtenemos la localizacion
        try {
            // Operaciones de la tarea en el servicio
            enviarResultado("Este es un mensaje desde el servicio número 0");
            Thread.sleep(3000);
            enviarResultado("Este es un mensaje desde el servicio número 1");
            Thread.sleep(3000);
            enviarResultado("Este es un mensaje desde el servicio número 2");
            Thread.sleep(3000);

        } catch (InterruptedException e) {
            // Restore interrupt status.
            Thread.currentThread().interrupt();
        }
        // fin del servicio
        stopSelf();
    }

    // Enviar un mensaje desde el servicio
    public void enviarResultado(String message) {
        Intent intent = new Intent(ServicioSimple.SERVICE_RESULT);
        if (message != null)
            intent.putExtra(ServicioSimple.SERVICE_MESSAGE, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    // Inicio del servicio
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Toast.makeText(this, "Inicio de servicios", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    // Final del servicio
    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Fin de servicio", Toast.LENGTH_SHORT).show();
    }
}
